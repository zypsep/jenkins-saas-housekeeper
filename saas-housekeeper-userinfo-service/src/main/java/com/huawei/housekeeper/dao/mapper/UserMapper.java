/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.huawei.housekeeper.dao.entity.User;

/**
 * @author l84165417
 * @since 2022/1/26 17:25
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
    /**
     * @param userName 用户名
     * @return 用户信息
     */
    User getByUserName(String userName);

    List<User> selectPageUser(IPage<User> page, String userName);
}

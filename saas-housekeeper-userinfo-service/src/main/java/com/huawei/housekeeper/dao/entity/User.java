/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.dao.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huawei.housekeeper.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @Author l84165417
 * @Date 2022/1/26 17:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("user")
public class User extends BaseEntity {
    /**
     * 用户ID
     */
    @TableField("user_id")
    private String userId;

    /**
     * 用户名
     */
    @TableField("USER_NAME")
    private String userName;

    /**
     * 密码
     */
    @TableField("PASSWORD")
    private String password;

    /**
     * 用户角色
     */
    @TableField("USER_ROLE")
    private Integer userRole;

    /**
     * 默认地址
     */
    @TableField("DEFAULT_ADDRESS")
    private String defaultAddress;

    /**
     * 电话号码
     */
    @TableField("PHONE_NO")
    private String phoneNo;

    /**
     * 邮箱
     */
    @TableField("EMAIL")
    private String email;

    /**
     * 身份证号码
     */
    @TableField("ID_CARD")
    private String idCard;

    /**
     * 账户类型
     */
    @TableField("ACCOUNT_TYPE")
    private Integer accountType;

    /**
     * 删除标志
     */
    @TableField("delete_flag")
    private String deleteFlag;

}

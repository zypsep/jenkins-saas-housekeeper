/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper;

import lombok.extern.log4j.Log4j2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 主启动类
 *
 * @author y00464350
 * @since 2022-02-14
 */
@Log4j2
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.huawei.housekeeper"})
public class SaasUserInfoServiceApplication {

    public static void main(String[] args) {
        log.info("user info module start...");
        SpringApplication.run(SaasUserInfoServiceApplication.class, args);
        log.info("user info module start over...");
    }
}

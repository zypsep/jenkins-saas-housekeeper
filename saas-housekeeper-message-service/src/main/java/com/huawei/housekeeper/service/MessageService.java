/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.service;

import java.util.List;

import com.huawei.housekeeper.dao.entity.Message;

/**
 * 消息接口
 *
 * @since 2022-02-15
 */
public interface MessageService {

    /**
     * 获取所有消息
     *
     * @return 消息列表
     */
    List<Message> getAllMsg();

    /**
     * 获取指定状态消息
     *
     * @param status 状态
     * @return 消息列表
     */
    List<Message> getMsg(Integer status);

    /**
     * 获取消息数量
     *
     * @return 消息数量
     */
    int getMsgCount();

    /**
     * 修改消息状态
     *
     * @param idList 消息id
     * @param status 消息状态
     * @return 是否修改成功
     */
    boolean setMsgStatus(List<String> idList, Integer status);
}

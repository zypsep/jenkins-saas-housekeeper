/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.feign;

import com.huawei.housekeeper.result.ListRes;
import com.huawei.housekeeper.feign.request.PageQueryAllUserDto;
import com.huawei.housekeeper.feign.response.GetTenantUserInfoVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * userinfo远程调用
 *
 * @author cwx1109128
 * @since 2022-09-06
 */
@Component
@FeignClient(name = "saas-user-info", path = "/user")
public interface UserInfoService {

    @PostMapping("/tenant/allUserInfo")
    ListRes<GetTenantUserInfoVo> getAllUserInfoList(PageQueryAllUserDto pageQueryUserDto);

}

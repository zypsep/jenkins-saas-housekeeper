/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 抢单或完成任务
 *
 * @author jwx1116205
 * @since 2022-03-02
 */
@Setter
@Getter
@ApiModel("抢单或完成任务对象")
public class DoTaskDto {
    @NotNull(message = "任务id必填")
    @Min(value = 1, message = "最小值:1")
    @ApiModelProperty(value = "任务Id", required = true)
    private Long id;

    @NotBlank(message = "任务行为必填")
    @Length(max = 50, message = "最大长度:50")
    @ApiModelProperty(value = "抢单：accept,任务完成：finished,任务取消：cancel", required = true)
    private String action;

}
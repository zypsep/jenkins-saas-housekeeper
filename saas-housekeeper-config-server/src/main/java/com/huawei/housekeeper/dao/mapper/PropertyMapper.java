/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.dao.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.huawei.housekeeper.controller.request.SetPropertyDto;

/**
 * 增加配置
 *
 * @author y00464350
 * @since 2022-03-24
 */
@Mapper
public interface PropertyMapper {

    int insertProperties(SetPropertyDto setPropertyDto);

}

/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.response;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * 查询用户响应vo
 *
 * @author wwx1136431
 * @since 2022/3/2 15:18
 */
@Getter
@Setter
@JsonIgnoreProperties(value = {"code", "message"})
public class GetWorkUserInfoVo {
    @JsonProperty(value = "result")
    private UserInfo userInfo;

    @Data
    public static class UserInfo {
        /**
         * 用户ID
         */
        @JSONField(name = "userId")
        private String userId;

        /**
         * 用户名
         */
        @JSONField(name = "userName")
        private String userName;

        /**
         * 电话号码
         */
        @JSONField(name = "phoneNo")
        private String phoneNo;
    }
}

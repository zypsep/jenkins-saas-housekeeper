
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * 用户下单Dto
 *
 * @author lWX1128557
 * @since 2022-02-23
 */
@Data
@ApiModel(value = "用户下单")
public class CreateOrderDto {
    @NotBlank
    @Length(max = 32, message = "最大长度：32")
    @ApiModelProperty(value = "顾客姓名")
    private String customerName;

    @NotNull
    @ApiModelProperty(value = "可选服务表id")
    private Integer skuId;

    @NotBlank(message = "必填")
    @Length(max = 255, message = "最大长度：255")
    @ApiModelProperty(value = "服务地址")
    private String address;

    @NotBlank(message = "必填")
    @Pattern(regexp = "^1[0-9]{10}$", flags = Pattern.Flag.CANON_EQ)
    @Length(min = 11, max = 11, message = "长度不正确")
    @ApiModelProperty(value = "用户电话")
    private String customerPhone;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "约定时间")
    private Date appointmentTime;

    @NotNull
    @Min(value = 1, message = "最小值：1")
    @ApiModelProperty(value = "数量")
    private Integer amount;

    @ApiModelProperty(value = "备注")
    private String remark;
}

/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.response;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 租户订单列表Vo
 *
 * @author lWX1128557
 * @since 2022-02-23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "租户查询订单列表")
public class GetOrdersOfTenantVo {
    @ApiModelProperty(value = "订单id")
    private Long orderId;

    @ApiModelProperty(value = "订单编号")
    private String orderNumber;

    @ApiModelProperty(value = "服务详情")
    private ServiceDetail serviceDetail;

    @ApiModelProperty(value = "订单价格")
    private BigDecimal payment;

    @ApiModelProperty(value = "顾客姓名")
    private String customerName;

    @ApiModelProperty(value = "工人姓名")
    private String serverName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "预约时间")
    private Date appointmentTime;

    @ApiModelProperty(value = "订单状态")
    private Integer status;

    @ApiModelProperty(value = "服务图片")
    private String imgSrc;

    /**
     * 服务详情
     *
     * @author lWX1128557
     * @since 2022-03-01
     */
    @Data
    public static class Specifications {
        @ApiModelProperty(value = "规格名称")
        private String specName;

        @ApiModelProperty(value = "规格选项")
        private String optionName;
    }

    /**
     * 服务规格
     *
     * @author lWX1128557
     * @since 2022-03-01
     */
    @Data
    public static class ServiceDetail {
        @ApiModelProperty(value = "服务名称")
        private String serviceName;

        @ApiModelProperty(value = "服务规格")
        private List<Specifications> specifications;

        @ApiModelProperty(value = "服务价格")
        private BigDecimal price;
    }
}

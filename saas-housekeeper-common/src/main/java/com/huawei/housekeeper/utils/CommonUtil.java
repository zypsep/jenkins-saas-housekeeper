/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package com.huawei.housekeeper.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 通过方法不好分类的都放在这里
 *
 * @since 2021-12-03
 */
public class CommonUtil {
    /**
     * 获取 UUID 除去 "-"
     *
     * @return
     */
    public static String getUUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    /**
     * 初始化headerMap
     *
     * @param tenantDomain
     * @return headerMap
     */
    public static Map<String, Object> buildHeaderMap(String tenantDomain) {
        Map<String, Object> headerMap = new HashMap<>();
        headerMap.put("tenantDomain", tenantDomain);
        return headerMap;
    }
}
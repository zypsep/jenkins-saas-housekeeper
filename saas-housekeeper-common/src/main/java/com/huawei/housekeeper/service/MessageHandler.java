package com.huawei.housekeeper.service;

import com.huawei.housekeeper.entity.BaseMqMessage;
import lombok.extern.java.Log;
import org.apache.rocketmq.spring.core.RocketMQListener;
@Log
public abstract class MessageHandler<T extends BaseMqMessage> implements RocketMQListener<T> {

    /**
     * 具体业务处理,业务中只需要实现这个方法
     * @param message
     * @throws Exception
     */
    public void handleMessage(T message) throws Exception{

    }

    /**
     * 公共处理部分内容
     * @param message
     */
    public void dispatchMessage(T message){

        try{
            handleMessage(message);
        } catch (Exception e) {
            log.info("message with key:"+message.getKey()+" handle error");
        }

    }

    /**
     * 实现RocketMQListener方法
     * @param message
     */
    @Override
    final public void onMessage(T message){
        dispatchMessage(message);
    }


}

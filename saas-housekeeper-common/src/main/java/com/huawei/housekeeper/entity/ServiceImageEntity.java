package com.huawei.housekeeper.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


@Data
public class ServiceImageEntity extends BaseEntity {

    /**
     * 图片ID
     */
    @TableField("image_id")
    private String imgSrc;
    /**
     * 图片名称
     */
    @TableField("image_name")
    private String imageName;

    /**
     * 服务ID
     */
    @TableField("service_id")
    private String serviceId;
    /**
     * 图片类型
     */
    @TableField("type")
    private String type;

    /**
     * 图片内容
     */
    @TableField("content")
    private byte[] content;

}

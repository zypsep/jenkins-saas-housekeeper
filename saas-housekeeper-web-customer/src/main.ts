import { createApp } from 'vue';
import { createRouter, createWebHashHistory } from 'vue-router';
import ElementPlus from 'element-plus';
import { routerGuard, directiveLog } from '@/utils/utils';
import selfRoutes from './router';
import App from './App.vue';
import '@/assets/style/default.css';
import 'element-plus/dist/index.css';

const router = createRouter({
    history: createWebHashHistory(),
    routes: selfRoutes,
});

const app = createApp(App);
app.use(ElementPlus);
app.use(router);
routerGuard(router);
directiveLog(app);
app.mount('#app');

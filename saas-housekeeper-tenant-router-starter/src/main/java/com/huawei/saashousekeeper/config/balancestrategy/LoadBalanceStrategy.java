/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.config.balancestrategy;

import com.huawei.saashousekeeper.config.dynamicdatasource.SnapshotDataSource;

import java.util.List;

/**
 * 数据源选则负责均衡策略
 *
 * @author lWX1156935
 * @since 2022/4/22
 */
public interface LoadBalanceStrategy {
    /**
     * 获取负载均衡策略
     *
     * @return 策略
     */
    String getStrategyType();

    /**
     * 获取数据源
     *
     * @param dataSourceList 数据源列表
     * @return 数据源
     */
    SnapshotDataSource get(List<SnapshotDataSource> dataSourceList);
}

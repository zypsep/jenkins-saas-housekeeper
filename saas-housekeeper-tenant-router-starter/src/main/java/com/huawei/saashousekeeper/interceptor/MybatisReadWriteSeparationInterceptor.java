/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.interceptor;

import com.huawei.saashousekeeper.context.TenantContext;
import com.huawei.saashousekeeper.constants.Constants;

import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

/**
 * mybatis拦截，选主从
 *
 * @author lWX1156935
 * @since 2022/4/22
 */
@Intercepts({
    @Signature(type = Executor.class, method = "query",
        args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class}),
    @Signature(type = Executor.class, method = "query", args = {
        MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class
    }), @Signature(type = Executor.class, method = "update", args = {MappedStatement.class, Object.class})
})
public class MybatisReadWriteSeparationInterceptor implements Interceptor {
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        MappedStatement statement = (MappedStatement) invocation.getArgs()[0];

        // 读写分离，除了查询，其他都走主库
        String dbStrategy = SqlCommandType.SELECT.equals(statement.getSqlCommandType())
            ? Constants.DB_SLAVE
            : Constants.DB_MASTER;

        // 记录本次操作库,用于本次会话的 本次操作以及后续操作的选库参考
        TenantContext.setDbStrategyType(dbStrategy);
        return invocation.proceed();
    }
}

/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.constants;

/**
 * 日志埋点常量
 *
 * @author wx1136431
 * @since 2022-04-21
 */
public interface Constants {
    /**
     * 租户id
     */
    String TENANT_ID = "tenantId";

    /**
     * 租户标识
     */
    String TENANT_DOMAIN = "tenantDomain";
    /**
     * 租户id
     */
    String USER_ID = "tenantId";
    /**
     * 主库
     */
    String DB_MASTER = "master";

    /**
     * 从库
     */
    String DB_SLAVE = "slave";

    /**
     * get方法前缀
     */
    String METHOD_GET_PREFIX = "get";

    /**
     * 下划线
     */
    String UNDERLINE = "_";

    /**
     * 多种连接池配置属性路径
     */
    String MULTI_POOL_PROPERTY_APPEND = "pools";

    /**
     * 数据源组配置路径
     */
    String DATA_SOURCE_MAP = "data-source-map";

    /**
     * 分隔符
     */
    String DOT = ".";

    /**
     * 问号
     */
    String QUEST_MARK = "\\u003F";
}

/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.dao.mapper;

import com.huawei.housekeeper.controller.response.GetServiceSelectionByServiceIdVo;
import com.huawei.housekeeper.dao.entity.ServiceSelection;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 选集表Mapper接口
 *
 * @author jwx1116205
 * @since 2022-03-03
 */
@Mapper
public interface ServiceSelectionMapper extends BaseMapper<ServiceSelection> {
    int deleteByOptionIds(List<Long> optionIds, String updatedBy);

    int deleteBySkuIds(List<Long> skuIds, String updatedBy);

    int insertBatch(List<ServiceSelection> records);

    List<GetServiceSelectionByServiceIdVo> getServiceSelectionVoListByServiceId(Long serviceId);
}
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 功能描述
 *
 * @author jWX1116205
 * @since 2022-02-16
 */
@Setter
@Getter
@ApiModel("根据skuId拿到服务详细描述")
public class GetSkuDetailBySkuIdDto {
    @NotNull(message = "skuId必填")
    @Min(value = 1L, message = "最小值：1")
    @ApiModelProperty(value = "服务ID", required = true)
    private Long skuId;
}
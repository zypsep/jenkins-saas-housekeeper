/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

create table `t_order`
(
    id               int(32)      not null primary key AUTO_INCREMENT comment '主键',
    order_number     varchar(255) not null comment '订单号',
    customer_id      varchar(32) comment '顾客ID',
    customer_name    varchar(32) comment '顾客姓名',
    customer_phone   varchar(255) comment '顾客电话',
    sku_id           int(32) comment '可选服务表sku_id',
    price            decimal(24, 6) comment '订单价格，服务中心获取',
    appointment_time datetime comment '服务时间',
    amount           int comment '数量',
    remark           varchar(255) comment '备注',
    service_detail   text(21839) comment '服务细节，服务中心获取',
    payment          decimal(24, 6) comment '订单价格，价格乘以数量',
    address          varchar(255) comment '服务地址',
    `status`         int(32) comment '状态',
    employee_id      bigint comment '工人id',
    employee_name    varchar(32) comment '工人名称',
    employee_phone   varchar(255) comment '工人电话',
    service_img      varchar(255) comment '服务图片',
    created_by       varchar(90) comment '创建人',
    created_time     datetime comment '创建时间',
    updated_by       varchar(90) comment '更新人',
    updated_time     datetime comment '更新时间'
) comment = '订单表'
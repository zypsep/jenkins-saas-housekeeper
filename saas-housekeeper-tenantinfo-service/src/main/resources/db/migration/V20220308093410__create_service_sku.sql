/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

CREATE TABLE `t_service_sku`
(
    `id`           int                                                    NOT NULL AUTO_INCREMENT COMMENT '可选服务ID',
    `service_id`   int                                                    NULL DEFAULT NULL COMMENT '服务ID',
    `price`        decimal(24, 6)                                         NULL DEFAULT NULL COMMENT '价格',
    `created_by`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `created_time` datetime                                               NULL DEFAULT NULL COMMENT '创建时间',
    `updated_by`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
    `updated_time` datetime                                               NULL DEFAULT NULL COMMENT '更新时间',
    `revision`     varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '乐观锁',
    `delete_flag`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '0' COMMENT '删除标志',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 60
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '可选服务表(SKU)'
  ROW_FORMAT = COMPACT;
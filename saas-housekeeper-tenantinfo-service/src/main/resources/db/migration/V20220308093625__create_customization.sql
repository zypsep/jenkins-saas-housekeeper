/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

CREATE TABLE `t_customization`
(
    `config_id`  int                                                    NOT NULL AUTO_INCREMENT COMMENT '配置ID',
    `style_flag` tinyint                                                NULL DEFAULT NULL COMMENT '主题标识',
    `store_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '店名',
    `tenant_id`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户标识',
    PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 13
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '租户配置表'
  ROW_FORMAT = Dynamic;
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.service;

import com.huawei.housekeeper.controller.request.AdminLoginDto;

/**
 * 管理员service层
 *
 * @author lWX1128557
 * @since 2022-03-03
 */
public interface AdminContextService {
    /**
     * 管理员登录
     *
     * @param adminLoginDto 管理员登录dto
     * @return token
     */
    String adminLogin(AdminLoginDto adminLoginDto);
}

/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 租户详情
 *
 * @author lWX1128557
 * @since 2022-03-02
 */
@Getter
@Setter
@ApiModel(value = "租户详情")
public class GetTenantDetailVo {
    @ApiModelProperty(value = "租户名")
    private String name;

    @ApiModelProperty(value = "租户企业信用码")
    private String number;

    @ApiModelProperty(value = "租户电话")
    private String phone;

    @ApiModelProperty(value = "租户邮箱")
    private String email;

    @ApiModelProperty(value = "租户标识")
    private String tag;

    @ApiModelProperty(value = "租户域名")
    private String domain;

    @ApiModelProperty(value = "租户状态")
    private Integer status;
}
